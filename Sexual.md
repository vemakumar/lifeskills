
# Understanding and Addressing Sexual Harassment 

## Identifying Sexual Harassment

* Unwanted Advances: Any unwelcome romantic or sexual gestures, advances, or propositions made towards an individual without their consent.
* Inappropriate Communication: Sharing sexually explicit jokes, comments, or remarks that create discomfort or contribute to a hostile work environment.
* Physical Intrusion: Unwanted physical contact, such as touching, groping, or invading personal space without consent.
* Online Misconduct: Cyberbullying tactics, including sending explicit messages or images without consent, which can cause emotional distress.
* Creating a Hostile Environment: Establishing an intimidating atmosphere with persistent sexual comments or behavior that undermines individuals' sense of safety.

# Steps to Address Sexual Harassment

## Responding to Sexual Harassment Incidents

* Speak Up: Assertively communicate that the behavior is unacceptable, emphasizing the need for respect and professional conduct.
* Document Incidents: Keep detailed records of the harassment incidents, including dates, times, locations, and any witnesses involved.
* Report Promptly: Immediately report the harassment to HR, management, or a designated authority, following company reporting procedures.
* Seek Support: Seek support from HR, employee assistance programs, or external resources to address the emotional impact of harassment.
* Know Your Rights: Understand your legal rights and protections against sexual harassment, empowering yourself to take appropriate action.

## Understanding Different Forms of Sexual Harassment

* Unwanted Advances: Unsolicited romantic or sexual overtures that create discomfort or distress for the recipient.
* Inappropriate Communication: Sharing sexually explicit jokes, comments, or remarks that contribute to a hostile work environment or discomfort among coworkers.
* Physical Intrusion: Any form of unwanted physical contact, including touching, groping, or invasion of personal space without consent.
* Online Misconduct: Sending explicit messages or images without consent, cyberbullying, or engaging in inappropriate behavior online.
* Creating a Hostile Environment: Establishing an environment filled with persistent sexual comments or behavior that undermines individuals' sense of safety and wel*being.

## Practicing Appropriate Behavior

* Respect Boundaries: Obtain explicit consent before engaging in any physical or sexual activities, respecting individuals' autonomy and personal space.
* Use Professional Language: Refrain from using sexually explicit language, making inappropriate jokes, or engaging in behavior that could create discomfort or harassment.
* Listen Actively: Listen to concerns or complaints about harassment with empathy, taking them seriously and responding appropriately.
* Report Concerns: Report instances of sexual harassment or inappropriate behavior to HR or management, ensuring a safe and respectful workplace for all employees.
* Support Victims: Offer support and assistance to victims of sexual harassment, helping them access resources and support services to address their needs effectively.

